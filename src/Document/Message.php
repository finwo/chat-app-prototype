<?php

namespace Finwo\ChatApp\Document;

class Message extends AbstractDocument {

  /**
   * @var string
   */
  public $from;

  /**
   * @var string|null
   */
  public $to;

  /**
   * @var int
   */
  public $when;

  /**
   * @var string
   */
  public $data;

}
