<?php

namespace Finwo\ChatApp\Document;

class Account extends AbstractDocument {

  /**
   * @var string
   */
  public $username;

  /**
   * @var string
   */
  protected $passhash;

  /**
   * Verify a password
   *
   * @var string $password
   *
   * @return boolean
   */
  public function verifyPassword( $password ) {
    return md5($password) === $this->passhash;
  }

  public function setPasshash( $passhash ) {
    $this->passhash = $passhash;
  }


}
