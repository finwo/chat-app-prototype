<?php

namespace Finwo\ChatApp\Document;

use Finwo\ChatApp\Db;

abstract class AbstractDocument {

  /**
   * @var int
   */
  public $id = null;

  /**
   * @param  array  $data  Data to insert into the account
   */
  public function __construct( $data = null ) {
    if (!is_null($data)) {

      if (is_array($data)) {
        // TODO: properly map into an stdObject
        $data = json_decode(json_encode($data));
      }

      Db::mapper()->map($data, $this );
    }
  }

  /**
   * @return  string    Returns which table the document belongs to
   */
  public static function table() {
    $class = get_called_class();
    $short = @array_pop(explode('\\',$class));
    return strtolower($short);
  }

  public static function get( $id ) {
    $class = get_called_class();
    $db    = Db::instance();
    $row   = $db->table( self::table(), $id );
    return Db::mapper()->map($row, $class);
  }

  public static function findOne( $column, $value ) {
    $class = get_called_class();
    $db    = Db::instance();
    $table = $db->table( self::table() );
    $row   = $table->where( $column, $value )->fetch();

    if (!$row->exists()) {
      return null;
    }

    return Db::mapper()->map($row, new $class());
  }

  public static function query() {
    return Db::instance()->table(self::table());
  }

  public function save() {
    $db = Db::instance();

    // New document
    if (is_null($this->id)) {

      // Prepare data
      $bare = get_object_vars($this);
      unset($bare['id']);

      // Insert into table
      $row = $db->createRow( self::table(), $bare );
      $row->save();

      // Map back auto-filled fields
      Db::mapper()->map($row, $this);

      return $this;
    }

    // Existing document

    // Prepare data
    $bare = get_object_vars($this);

    // Fetch original and save new data
    $row  = $db->table( self::table(), $this->id );
    $row->update($bare);

    // Map back auto-filled fields
    Db::mapper()->map($row, $this);

    return $this;
  }

}
