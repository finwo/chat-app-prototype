<?php

namespace Finwo\ChatApp;

use Finwo\ChatApp\Document\Account;
use Finwo\ChatApp\Document\Message;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class PacketHandler implements MessageComponentInterface {
    protected $clients;
    protected $users;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        $this->users   = array();
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {

      // Decode the message
      $msg = json_decode($msg, true);
      if (!array_key_exists('action',$msg)) {
        // Message does not contain action
        // Notify the sender it was corrupt
        $from->send(json_encode([
          'action'      => 'error',
          'error'       => 'message-corrupt',
          'description' => 'The message received by the server was corrupt or did not contain an action field.',
        ]));
        return;
      }

      switch($msg['action']) {
        case 'login':

          if (!isset($msg['username'])) {
            $from->send(json_encode([
              'action'      => 'error',
              'error'       => 'login-missing-username',
              'description' => 'The login action requires a username to be given',
            ]));
            return;
          }

          if (!isset($msg['password'])) {
            $from->send(json_encode([
              'action'      => 'error',
              'error'       => 'login-missing-password',
              'description' => 'The login action requires a password to be given',
            ]));
            return;
          }

          // Fetch the requested account
          $account = Account::findOne( 'username', $msg['username'] );
          $valid   = !is_null($account);

          // Verify password if account was found
          if ($valid) {
            $valid = $valid && $account->verifyPassword( $msg['password'] );
          }

          // Don't show if the account was found
          if (!$valid) {
            $from->send(json_encode([
              'action'      => 'error',
              'error'       => 'login-invalid-credentials',
              'description' => 'The username/password combination could not be found',
            ]));
            return;
          }

          // Approve this account/connection combination
          // TODO: nicer user tracking
          $this->users[$account->username] = $from;

          // Execution being here = login approved
          $from->send(json_encode([
            'action'  => 'authenticate',
            'account' => $account,
          ]));

          // Send 10 last messages
          $rows = Message::query()
            ->where('to', null)
            ->orderBy('when', 'DESC')
            ->limit(20)
            ->fetchAll();
          foreach($rows as $history) {
            $history['action'] = 'message';
            $from->send(json_encode($history));
          }

          // Send 10 last sent private messages
          $rows = Message::query()
            ->where('from', $account->username)
            ->whereNot('to', null)
            ->orderBy('when', 'DESC')
            ->limit(20)
            ->fetchAll();
          foreach($rows as $history) {
            $history['action'] = 'message';
            $from->send(json_encode($history));
          }

          // Send 10 last received private messages
          $rows = Message::query()
            ->where('to', $account->username)
            ->orderBy('when', 'DESC')
            ->limit(20)
            ->fetchAll();
          foreach($rows as $history) {
            $history['action'] = 'message';
            $from->send(json_encode($history));
          }

          break;

        case 'message':

          // Ensure there's a username to check
          if (!isset($msg['from'])) {
            $from->send(json_encode([
              'action'      => 'error',
              'error'       => 'message-missing-username',
              'description' => 'Sending a message requires a username',
            ]));
            return;
          }

          // Not logged in = discard
          if (
            (!array_key_exists($msg['from'], $this->users)) ||
            ($from !== $this->users[$msg['from']])
          ) {
            $from->send(json_encode([
              'action'      => 'error',
              'error'       => 'message-unauthenticated',
              'description' => 'Sending a message requires the user to be logged in',
            ]));
            return;
          }

          // The server dictates time
          $msg['when'] = time() * 1000;
          $encoded     = json_encode($msg);
          $message     = new Message($msg);
          $message->save();

          // Send to private participants
          if (isset($msg['to'])) {
            if (array_key_exists($msg['to'], $this->users)) {
              $this->users[$msg['to']]->send($encoded);
            }
            $from->send($encoded);
            return;
          }

          // Or to all
          foreach ( $this->clients as $client ) {
            $client->send($encoded);
          }

          break;

        default:
          var_dump('UNKNOWN: ' . json_encode($msg));
          // Action not supported by this server
          // Notify the sender the action is unknown
          $from->send(json_encode([
            'action'      => 'error',
            'error'       => 'action-unknown',
            'description' => 'Unknown action received by the server: ' . $msg['action'],
          ]));
          break;
      }
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        // TODO: nicer user tracking
        unset($this->users[array_search($conn,$this->users)]);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";

        $conn->close();
    }

}
