<?php

namespace Finwo\ChatApp;

use \PDO;
use \JsonMapper;
use LessQL\Database;

class Db {
  public static function pdo() {
    static $pdo = null;

    // Load db if not there yet
    if(is_null($pdo)) {
      $pdo = new \PDO('sqlite:chatapp.db');
    }

    return $pdo;
  }

  public static function instance() {
    static $db  = null;

    // Initialize orm
    if(is_null($db)) {
      $db = new Database(self::pdo());
    }

    return $db;
  }

  public static function mapper() {
    static $mapper = null;

    if(is_null($mapper)) {
      $mapper = new JsonMapper();
    }

    return $mapper;
  }
}
