#!/usr/bin/env php
<?php

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Finwo\ChatApp\PacketHandler;
use Finwo\ChatApp\Db;

use Finwo\ChatApp\Document\Account;

require dirname(__DIR__).'/vendor/autoload.php';

// Fetch DB and base data
$pdo      = Db::pdo();
$db       = Db::instance();
$basedata = require dirname(__DIR__).'/basedata.php';
$pdo->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

// Ensure the schema is in the database
foreach($basedata['schema'] as $tableName => $columns) {
  $sql = implode(' ', [
    "CREATE TABLE IF NOT EXISTS $tableName (",
    implode(', ', $columns),
    ")"
  ]); 
  $pdo->exec($sql);
}

// Insert basedata in db if none found
foreach($basedata['data'] as $tableName => $rows) {
  $table = $db->table($tableName);
  $found = $table->fetch();
  if (is_null($found)) {
    $table->insert($rows);
  }
}

$server = IoServer::factory(
  new HttpServer(
    new WsServer(
      new PacketHandler()
    )
  ),
  3000
);

$server->run();
