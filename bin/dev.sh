#!/usr/bin/env bash

# Ensure bash in approot
[ "$BASH" ] || exec bash "$0" "$@"
cd "$(dirname $0)/.."

# Start static server
php -S localhost:5000 -t public/ &
SRVPID=$!

# Start websocket server
bin/server-ws.php

# Send ^C to static server
kill -INT $SRVPID
