<?php

// TODO: read schema from document classes
// NOTE: in a real environment, md5-hashed passwords are far from adequate
return [
  'schema' => [
    'account' => [
      '`id` INTEGER PRIMARY KEY AUTOINCREMENT',
      '`username` VARCHAR(64) UNIQUE NOT NULL',
      '`passhash` VARCHAR(64) NOT NULL',
    ],
    'message' => [
      '`id` INTEGER PRIMARY KEY AUTOINCREMENT',
      '`from` VARCHAR(64) NOT NULL',
      '`to` VARCHAR(64) NULL',
      '`when` BIGINT(11) NOT NULL',
      '`data` VARCHAR(240) NOT NULL',
    ],
  ],
  'data' => [
    'account' => [
      [
        'id'       => 1,
        'username' => 'finwo',
        'passhash' => md5('pizza calzone'),
      ],
      [
        'id'       => 2,
        'username' => 'some1',
        'passhash' => md5('else'),
      ],
      [
        'id'       => 3,
        'username' => 'root',
        'passhash' => md5('supersecret'),
      ],
    ],
    'message' => [
      [
        'id'   => 1,
        'from' => 'some1',
        'when' => time() - 20,
        'data' => 'Hi there',
      ],
      [
        'id'   => 2,
        'from' => 'finwo',
        'when' => time() - 10,
        'data' => 'Hello back',
      ],
      [
        'id'   => 3,
        'from' => 'finwo',
        'to'   => 'root',
        'when' => time(),
        'data' => 'This some1 guy is weird',
      ],
    ],
  ]
];
