const rivets = require('rivets');
const data   = window.app = {};

// Custom formatters
rivets.formatters.date = timestamp => new Date(parseInt(timestamp)).toLocaleString();

app.account = null;

app.messages = [];

rivets.bind(document.body, data);

app.action = {
  authenticate({ account }) {
    app.account  = account;
    app.messages = [];
    app.cmd.help();
  },
  message( message ) {
    message.fromMe = message.from && (message.from === app.account.username);
    message.toMe   = message.to   && (message.to   === app.account.username);

    app.messages.push(message);

    app.messages.sort(function(left, right) {
      return left.when - right.when;
    });

    while( app.messages.length > 20 ) {
      app.messages.shift();
    }
  },
  showhelp() {
    app.cmd.help();
  },
};

app.cmd = {
  msg( fullstring ) {
    let   tokens = fullstring.split(' ');
    const to     = tokens.shift();
    const msg    = tokens.join(' ');

    // Regular group messages
    app.conn.send(JSON.stringify({
      action : 'message',
      from   : app.account.username,
      to     : to,
      data   : msg,
    }));

  },
  system( text ) {
    app.action.message({
      from: '',
      when: Date.now(),
      data: text,
    });
  },
  help() {
    app.cmd.system('Commands:');
    app.cmd.system('  /msg  username  Send private message');
    app.cmd.system('  /help           Show this help text');
  },
};

app.conn = new WebSocket('ws://localhost:3000');
app.conn.onopen = function(e) {
    console.log("Connection established!");
};
app.conn.onmessage = function(e) {
  console.log('msg', e.data);
  const data = JSON.parse(e.data);
  if(!data) return;
  if (!('action' in data)) return;
  if (!(data.action in app.action)) return;
  app.action[data.action](data);
};

// TODO: encrypt credentials
app.login = function(form) {
  app.conn.send(JSON.stringify({
    action  : 'login',
    username: form.username.value,
    password: form.password.value,
  }));
  return false;
};

app.send = function(form) {
  let msg = form.message.value;
  form.message.value = '';

  // Handle commands
  if (msg.substr(0,1) === '/') {
    let tokens  = msg.substr(1).split(' ');
    let command = tokens.shift();
    msg         = tokens.join(' ');
    if (!(command in app.cmd)) {
      command = 'help';
    }
    app.cmd[command](msg);
    return false;
  }

  // Regular group messages
  app.conn.send(JSON.stringify({
    action : 'message',
    from   : app.account.username,
    data   : msg,
  }));

  return false;
};
