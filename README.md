# Chat app

Simple chat app

## Setup

```sh
composer install
```

## Start

```sh
bin/dev.sh
```

## Development

```sh
composer install
( cd client && npm install && npm run build )
```
